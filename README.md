BOA setup by vagrant
==

Setup a BOA (Barracuda + Octopus + Aegir) server using Vagrant and Chef.

Requirements
-- 

- http://vagrantup.com/
- http://virtualbox.com/


Before Starting
--
To receive email from your vagrant based BOA server, change the command "Run the BOA Installer o1" in cookbooks > barracuda > recipes > default.rb

    command "boa in-stable local me@local.dev min" <-- set to your preferred email address

Other installation options exist, full list available on [http://drupalcode.org/project/barracuda.git/blob/HEAD:/docs/INSTALL.txt Drupal.org].

No edits are required to the Vagrantfile before provisioning your new server.

Get Started
--

When ready to automatically install BOA locally, run the following (takes about 30 minutes).

    vagrant up


Shared Folders
--
As I don't use Aegir and instead leverage the boa script for a well-tuned nginx server, I set-up vhosts and add sites manually as follows:

* Add files to /var/www, which is mounted and shared at "www"
* Add databases via chive (e.g. http://chive.local if you've added to your hosts file) or via mysql command line
* Nginx's vhost config folder is mounted at "vhosts". 2 example config files are included.


Adding a new vhost
--

Whenever adding a new vhost, remember
* Add an appropriate entry in your Mac (/etc/hosts) or Windows (c:\windows\system32\drivers\etc\hosts).
* Reload nginx config in your boa server

    service nginx reload


Remote import
--

Remember to check whether ´/data/disk/o1/.drush/provision/remote_import´ has been deleted.

If you want to be able to do remote imports, you need to do the following manually:

Copy the key to the remote server (also see http://larsolesen.dk/node/358)

    ssh-copy-id -i .ssh/id_rsa.pub user@remote-server

Go to ´admin/hosting/features´under ´Experimental´ and add remote import.
Go to ´Servers´ and add server. Choose hostmaster.

