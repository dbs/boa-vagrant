Chef::Log.debug("Running barracuda recipe")

#remote_file "/tmp/BOA.sh" do
#  source "http://files.aegir.cc/BOA.sh.txt"
#  mode 00755
#end

#execute "/tmp/BOA.sh" do
#  creates "/usr/local/bin/boa"
#end

#execute "Run the BOA Installer o1" do
#  user "root"
#  command "boa in-head local me@local.dev min"
#end

user "o1" do
  supports :manage_home => true
  home "/data/disk/o1"
  shell "/bin/bash"
  action :modify
end

directory "/data/disk/o1/.ssh" do
  owner "o1"
  group "users"
  mode 00700
  recursive true
  action :create
end

execute "Add ssh key to user" do
  command "ssh-keygen -b 4096 -t rsa -N \"\" -f /data/disk/o1/.ssh/id_rsa"
  creates "/data/disk/o1/.ssh/id_rsa"
end

file "/data/disk/o1/.ssh/id_rsa" do
  owner "o1"
  group "users"
  mode 00600
  action :touch
end

file "/data/disk/o1/.ssh/id_rsa.pub" do
  owner "o1"
  group "users"
  mode 00600
  action :touch
end  

# Only necessary as long as there is a but
#remote_file "/tmp/fix-remote-import-hostmaster-o1.patch" do
#  source "https://raw.github.com/lsolesen/boa-vagrant/master/patches/fix-remote-import-hostmaster-o1.patch"
#  mode 00755
#end

#execute "Apply Remote Import hostmaster patch" do
#  user "root"
#  cwd "/data/disk/o1/.drush/provision/remote_import"
#  command "patch -p1 < /tmp/fix-remote-import-hostmaster-o1.patch"
#end

execute "Run BOA Tool to fix permissions" do
  user "root"
  command "bash /var/xdrago/daily.sh"
end

execute "Use Drush 6.x" do
  user "root"
  command "rm /usr/bin/drush && ln -s /opt/tools/drush/6/drush/drush /usr/bin/drush"
end

execute "Add Site Audit command to Drush 6.x" do
  user "root"
  cwd "/opt/tools/drush/6/drush/commands"
  command "wget -O - http://ftp.drupal.org/files/projects/site_audit-7.x-1.6.tar.gz|tar -xzp"
end

bash "Set vagrant as vhosts folder user" do
  cwd "/"
  user "root"
  code <<-EOH
    chown -R vagrant:vagrant /var/aegir/config/server_master/nginx
    EOH
  action :run
end


# Rebuild VirtualBox Guest Additions
# http://vagrantup.com/v1/docs/troubleshooting.html
execute "Rebuild VirtualBox Guest Additions" do
  command "sudo /etc/init.d/vboxadd setup"
end
