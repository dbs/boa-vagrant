maintainer        "Eric DenBraber, original work by Lars Olesen (lars@intraface.dk)"
maintainer_email  "ericrdb@gmail.com"
license           "BSD License"
description       "Install BOA for local development."
version           "0.2"
recipe            "barracuda::default", "Install BOA"

supports 'ubuntu'